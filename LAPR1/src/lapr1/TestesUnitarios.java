/*


 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;

public class TestesUnitarios {

    public static void main(String[] args) throws FileNotFoundException {

        correTestes();

    }

    public static void correTestes() throws FileNotFoundException {

        System.out.println("Teste ao método Normalizar: " + (test_Normalizar() ? "OK" : "NOK"));

        System.out.println("Teste ao método aX: " + (testaAx() ? "OK" : "NOK"));

        System.out.println("Teste ao método calculoYmax: " + (test_CalculoYMax() ? "OK" : "NOK"));

        System.out.println("Teste ao método vetorPrioridadesExato: " + (testaVetorPrioridadesExato() ? "OK" : "NOK"));

        System.out.println("Teste ao método vetorPrioridadeAproximado: " + (testaVetorPrioridadeAproximado() ? "OK" : "NOK"));

        System.out.println("Teste ao método CalculoIC: " + (test_CalculoIC() ? "OK" : "NOK"));

        System.out.println("Teste ao método CalculoRC: " + (test_CalculoRC() ? "OK" : "NOK"));

        System.out.println("Teste ao método escolheOpção: " + (test_EscolherOpcao() ? "OK" : "NOK"));

        System.out.println("Teste ao método arredondar: " + (test_Arredondar() ? "OK" : "NOK"));

        System.out.println("Teste ao método prioridadeComposta: " + (testaPrioridadeComposta() ? "OK" : "NOK"));

        System.out.println("Teste ao método multiplicação: " + (testaMultiplicaçao() ? "OK" : "NOK"));

        System.out.println("Teste ao método normalizarTOPSIS: " + (test_normalizarTopSis() ? "OK" : "NOK"));
     
        System.out.println("Teste ao método normalizadapesada: " + (test_normalizadapesada() ? "OK" : "NOK"));
        
        System.out.println("Teste ao método procurarMaior: " + (test_procurarMaior() ? "OK" : "NOK"));
        
        System.out.println("Teste ao método procurarMenor: " + (test_procurarMenor() ? "OK" : "NOK"));
        
        System.out.println("Teste ao método test_matrizDistancia: " + (test_matrizDistancia() ? "OK" : "NOK"));
        
        System.out.println("Teste ao método vetorSolucao: " + (testarVetorSolucao() ? "OK" : "NOK"));
        
        System.out.println("Teste ao método vetorFinal: " + (testaVetorFinal() ? "OK" : "NOK"));
    }

    public static boolean test_Normalizar() {
        double[][][] matrizNorm = new double[4][4][4];
        double matrizesTestes[][][] = {
            {
                {1, 0.5, 3},
                {2, 1, 4},
                {0.3333, 0.25, 1}
            },
            {
                {1, 0.25, 4, 0.167},
                {4, 1, 4, 0.25},
                {0.25, 0.25, 1, 0.2},
                {6, 4, 5, 1}
            },
            {
                {1.0, 2.0, 5.0, 1.0},
                {0.5, 1.0, 4.0, 0.25},
                {0.2, 0.333, 1.0, 0.25},
                {1.0, 0.5, 4.0, 1.0}

            },
            {
                {1.0, 0.25, 4.0, 0.167},
                {4.0, 1.0, 4.0, 0.25},
                {0.25, 0.25, 1.0, 0.2},
                {6.0, 4.0, 5.0, 1.0}

            },};

        double matrizesTestesNormFinal[][][] = {
            {
                {0.3, 0.286, 0.375},
                {0.6, 0.571, 0.5},
                {0.1, 0.143, 0.125}
            },
            {
                {0.089, 0.045, 0.286, 0.103},
                {0.356, 0.182, 0.286, 0.155},
                {0.022, 0.045, 0.071, 0.124},
                {0.533, 0.727, 0.357, 0.619}

            },
            {
                {0.37, 0.522, 0.357, 0.4},
                {0.185, 0.261, 0.286, 0.1},
                {0.074, 0.087, 0.071, 0.1},
                {0.37, 0.13, 0.286, 0.4}

            },
            {
                {0.089, 0.045, 0.286, 0.103},
                {0.356, 0.182, 0.286, 0.155},
                {0.022, 0.045, 0.071, 0.124},
                {0.533, 0.727, 0.357, 0.619}

            },};
        Matematica.normalizar(matrizesTestes, matrizNorm, 3, 4);

        boolean vf = true;
        for (int i = 0; i < matrizesTestesNormFinal.length; i++) {

            if (vf == true) {

                for (int j = 0; j < matrizesTestesNormFinal[i].length; j++) {

                    if (vf == true) {
                        for (int w = 0; w < matrizesTestesNormFinal[i][j].length; w++) {

                            if (Math.abs(matrizesTestesNormFinal[i][j][w] - matrizNorm[i][j][w]) < 0.001) {
                                vf = true;
                            } else {
                                vf = false;
                                break;
                            }
                        }
                    }
                }

            }

        }
        return vf;
    }

    public static boolean testaAx() {
        double a[][] = {{1, 0.5, 3},
        {2, 1, 4},
        {0.3333, 0.25, 1}};
        double vPrioridade[] = {0.32, 0.56, 0.12};
        boolean vf = true;
        double[] axfinal = {0.96, 1.68, 0.37};

        double[] axtemp = Matematica.aX(a, vPrioridade);

        for (int i = 0; i < axtemp.length; i++) {
            if (Math.abs(axtemp[i] - axfinal[i]) < 0.01) {
                vf = true;
            } else {
                vf = false;
                break;
            }
        }
        return vf;
    }

    public static boolean test_CalculoYMax() throws FileNotFoundException {
        Formatter testfout = new Formatter(new File("testes.txt"));
        boolean vf = true;
        double[][] a = {{1, 0.5, 3},
        {2, 1, 4},
        {0.3333, 0.25, 1}};
        double[] vPrioridade = {0.32, 0.56, 0.12};
        double YmaxFinal = 3.0182947072896305;

        double YmaxTemp = Matematica.calculoYmax(a, vPrioridade, 3, testfout);
        if (Math.abs(YmaxFinal - YmaxTemp) < 0.01) {
            vf = true;
        } else {
            vf = false;

        }
        return vf;
    }

    public static boolean testaVetorPrioridadesExato() {

        double matriz[][] = {{1, 0.5, 3},
        {2, 1, 4},
        {0.3333, 0.25, 1}};
        double vPrioridadefinal[] = {0.32, 0.56, 0.12};
        boolean vf = true;

        double[] vPrioridadeTemp = Matematica.vetorPrioridadesExato(matriz, 3);

        for (int i = 0; i < vPrioridadeTemp.length; i++) {
            if (Math.abs(vPrioridadeTemp[i] - vPrioridadefinal[i]) < 0.01) {
                vf = true;
            } else {
                vf = false;
                break;
            }
        }
        return vf;
    }

    public static boolean testaVetorPrioridadeAproximado() {

        double matriz[][] = {{0.3, 0.286, 0.375},
        {0.6, 0.571, 0.5},
        {0.1, 0.143, 0.125}};
        double vPrioridadefinal[] = {0.32, 0.56, 0.12};
        boolean vf = true;

        double[] vPrioridadeTemp = Matematica.vetorPrioridadeAproximado(matriz, 3);

        for (int i = 0; i < vPrioridadeTemp.length; i++) {
            if (Math.abs(vPrioridadeTemp[i] - vPrioridadefinal[i]) < 0.01) {
                vf = true;
            } else {
                vf = false;
                break;
            }
        }
        return vf;
    }

    public static boolean test_CalculoIC() throws FileNotFoundException {
        Formatter testfout = new Formatter(new File("testes.txt"));
        boolean vf = true;
        double[][] a = {{1, 0.5, 3},
        {2, 1, 4},
        {0.3333, 0.25, 1}};
        double[] vPrioridade = {0.32, 0.56, 0.12};
        double ICFinal = 0.009147353644815226;

        double ICTemp = Matematica.calculoIC(a, vPrioridade, 3, testfout);
        if (Math.abs(ICFinal - ICTemp) < 0.001) {
            vf = true;
        } else {
            vf = false;

        }
        return vf;
    }

    public static boolean test_CalculoRC() throws FileNotFoundException {
        Formatter testfout = new Formatter(new File("testes.txt"));
        boolean vf = true;
        double[][] a = {{1, 0.5, 3},
        {2, 1, 4},
        {0.3333, 0.25, 1}};
        double[] vPrioridade = {0.32, 0.56, 0.12};

        double RCFinal = 0.016;

        double RCTemp = Matematica.calculoRC(a, vPrioridade, 3, testfout);
        if (Math.abs(RCFinal - RCTemp) < 0.01) {
            vf = true;
        } else {
            vf = false;

        }
        return vf;
    }

    public static boolean test_EscolherOpcao() {
        boolean vf = true;
        double[] vprioridades = {0.277, 0.229, 0.422, 0.072};

        double maior = Matematica.escolherOpção(vprioridades);
        if (maior == 2) {
            vf = true;
        } else {
            vf = false;

        }
        return vf;
    }

    public static boolean test_Arredondar() {
        boolean vf = true;
        double arredondar = 4.43469260085583;
        double arredondadoFinal = 4.435;

        double arredondado = Matematica.arredondar(arredondar);
        if (arredondado == arredondadoFinal) {
            vf = true;
        } else {
            vf = false;

        }

        return vf;
    }

    public static boolean testaPrioridadeComposta() {
        double matriz[][] = {{0.32, 0.558, 0.122},
        {0.116, 0.247, 0.06, 0.577},
        {0.405, 0.215, 0.081, 0.299},
        {0.116, 0.247, 0.06, 0.577}};
        double prioridade_composta_final[] = {0.277, 0.229, 0.072, 0.422};
        boolean vf = true;

        double[] prioridade_composta = Matematica.prioridadeComposta(matriz, 3, 4);

        for (int i = 0; i < prioridade_composta.length; i++) {
            if (Math.abs(prioridade_composta[i] - prioridade_composta_final[i]) < 0.01) {
                vf = true;
            } else {
                vf = false;
                break;
            }
        }
        return vf;
    }

    public static boolean testaMultiplicaçao() {
        double matriz_prioridade[][] = {{0.116, 0.405, 0.116},
        {0.247, 0.215, 0.247},
        {0.06, 0.081, 0.06},
        {0.577, 0.299, 0.577}};
        double peso_criterio[] = {0.32, 0.558, 0.122};
        double prioridade_composta[] = {0.277, 0.229, 0.072, 0.422};
        boolean vf = true;

        double[] prioridade_composta_temp = Matematica.multiplicaçao(matriz_prioridade, peso_criterio, 4, 3);

        for (int i = 0; i < prioridade_composta_temp.length; i++) {
            if (Math.abs(prioridade_composta_temp[i] - prioridade_composta[i]) < 0.01) {
                vf = true;
            } else {
                vf = false;
                break;
            }
        }
        return vf;
    }

    public static boolean test_normalizarTopSis() {
        double[][] a = {{7, 8, 9, 6},
                        {9, 7, 6, 7},
                        {9, 8, 8, 8},
                        {8, 7, 9, 6}};
        double[][] normalizada = {{0.46157,0.5275,0.59344, .39563},
                                  {0.61379,0.4774,0.4092,0.4774},
                                  {0.5447,0.48418,0.48418,0.48418},
                                  {0.5275, 0.46157, 0.59344, 0.39563}};
        
        int n_opcoes = 4, n_criterios = 4;
        double normalizadaFinal[][] = new double[4][4];
        boolean vf = true;
        normalizadaFinal = Matematica.normalizarTOPSIS(a, n_opcoes, n_criterios);
        for (int i = 0; i < 4; i++) {
            if (vf == true) {
                for (int j = 0; j < 4; j++) {
                    if (Math.abs(normalizada[i][j] - normalizadaFinal[i][j]) < 0.001) {
                        vf = true;
                    } else {
                        vf = false;
                        break;
                    }
                }
            }
        }
        return vf;
    }

    public static boolean test_normalizadapesada() {
        boolean vf = true;
        double[][] normalizada = {{0.46157,0.5275,0.59344, .39563},
                                  {0.61379,0.4774,0.4092,0.4774},
                                  {0.5447,0.48418,0.48418,0.48418},
                                  {0.5275, 0.46157, 0.59344, 0.39563}};
        double[] vetorPesos = {0.1, 0.4, 0.3, 0.2};
        int n_opcoes = 4, n_criterios = 4;
        double normalizadaPesadaFinal[][] = {{0.04616, 0.05275, 0.05934,0.03956},
                                             { 0.24552, 0.19096, 0.16368, 0.19096},
                                             { 0.16341,0.14525, 0.14525,  0.14524},
                                               {0.1055, 0.09231, 0.11869, 0.07913}};
        
        double[][] normalizadaPesada = new double[4][4];
        normalizadaPesada = Matematica.normalizapesada(normalizada, vetorPesos, n_criterios, n_opcoes);
        for (int i = 0; i < 4; i++) {
            if (vf == true) {
                for (int j = 0; j < 4; j++) {
                    if (Math.abs(normalizadaPesada[i][j] - normalizadaPesadaFinal[i][j]) < 0.001) {
                        vf = true;
                    } else {
                        vf = false;
                        break;
                    }
                }
            }
        }
        return vf;
    }
    
    
    public static boolean test_procurarMaior(){
        
        double [][] matriz = {{0.048, 0.029, 0.01, 0.01},
                              {0.05, 0.020, 0.02, 0.02},
                              {0.442, 0.265, 0.088, 0.088},
                              {0.294, 0.177, 0.059, 0.059}};
        boolean vf = true;
        double maiorFinal = 0.048;
        
        double maiorTeste = TopSis.procurarMaior(matriz, 4, 0);
        
        if (Math.abs(maiorTeste - maiorFinal)<0.01) {
            vf = true;
        }else {
            vf = false;
        }
            return vf;
    }
    


    public static boolean test_procurarMenor() {
        
        double [][] matriz = {{0.048, 0.029, 0.01, 0.01},
                              {0.05, 0.020, 0.02, 0.02},
                              {0.442, 0.265, 0.088, 0.088},
                              {0.294, 0.177, 0.059, 0.059}};
        boolean vf = true;
        double menorFinal = 0.01;
        
        double menorTest = TopSis.procurarMenor(matriz, 4, 0);
        
        if (Math.abs(menorTest - menorFinal)<0.01) {
            vf = true;
        }else {
            vf = false;
        }
        return vf;
    }
    
    public static boolean test_matrizDistancia(){
        double [][]matriz = {{0.0484, 0.0291, 0.0097, 0.0097},
            {0.0499, 0.0199, 0.0199, 0.0100},
            {0.4419, 0.2652, 0.0884, 0.0884},
            {0.2945, 0.1767, 0.0589, 0.0589}};
        
        boolean vf = true;
        double [] vetorMaior = {0.0484, 0.0499, 0.4419, 0.2945};
        
        double [][]matrizFinal = {{0, 0.0194, 0.0388, 0.0388}, {0, 0.0299, 0.0299, 0.0399}, {0, 0.1768, 0.3536, 0.3536}, {0, 0.1178, 0.2356, 0.2356}};
       
        double [][]matriztemporaria = Matematica.matrizDistancia(matriz, vetorMaior, 4, 4);
        
        for (int i=0;i<4; i++){
            if (vf == true){
                for (int j=0; j<4; j++){
                    if (Math.abs(matriztemporaria[i][j] - matrizFinal[i][j])<0.001) {
                        vf = true;
                    } else {
                        vf = false;
                        break;
                    }    
                }
            }
        }
        return vf;
    }
    
    
    
    public static boolean testarVetorSolucao(){
        
        boolean vf = true;
        double[][] matrizdistancia = {{0, 0.0194, 0.0388, 0.0388},
                                      {0, 0.0299, 0.0299, 0.0399},
                                      {0, 0.1768, 0.3536, 0.3536},
                                      {0, 0.1178, 0.2356, 0.2356}};
        
        double [] vetorFinal = {0, 0.2154, 0.4277, 0.4285};
        
        double [] vetorTemp = Matematica.vetorSolucao(matrizdistancia, 4);
        
        for (int i = 0; i < 4; i++){
            if(Math.abs(vetorTemp[i] - vetorFinal[i]) < 0.001){
                vf = true;
            } else {
                vf = false;
                break;
            }
        }
        return vf;
    }
    
    
    public static boolean testaVetorFinal(){
        
        boolean vf = true;
        double[] vetorSolucaoIdealNegativa = {0.4285, 0.2135, 0.01, 0};
        double[] vetorSolucaoIdealPositiva = {0, 0.2154, 0.4277, 0.4285};
        double [] vetorFinal = {1, 0.4978, 0.0228, 0};
        
        double [] vetorTemp = Matematica.vetorFinal(vetorSolucaoIdealPositiva, vetorSolucaoIdealNegativa, 4);
        
        for (int i = 0; i < 4; i++){
            if(Math.abs(vetorTemp[i] - vetorFinal[i]) < 0.001){
                vf = true;
            } else {
                vf = false;
                break;
            }
        }
        return vf;
    }
}
