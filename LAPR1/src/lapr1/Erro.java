/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;
import java.util.Date;

public class Erro {

    public static void logErros(String mensagemErro) throws IOException {
        FileWriter f = new FileWriter("LogErros.txt", true);
        Formatter erro = new Formatter(f);
        Date data = new Date();
        
        erro.format(data.toString() + ": " + mensagemErro + "\n");

        erro.close();
        System.exit(0);
    }

    public static void consola(String mensagemErro) {
        
        System.out.println(mensagemErro + "\n");
        System.exit(0);
    }

    public static void logErros_consola(String mensagemErro) throws IOException {
        FileWriter f = new FileWriter("LogErros.txt", true);
        Formatter erro = new Formatter(f);
        Date data = new Date();

        erro.format(data.toString() + ": " + mensagemErro + "\n");
        System.out.println(mensagemErro + "\n");

        erro.close();
        
        System.exit(0);

    }

}
