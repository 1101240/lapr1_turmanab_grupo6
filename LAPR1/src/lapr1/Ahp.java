/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Scanner;


public class Ahp {

    //Configuração do método
    public static final int MAX_CRITERIOS = 100;

    public static void ahp(double l,  double s, String ficheiroEntrada, String ficheiroSaida) throws IOException {
        File ficheiro = new File(ficheiroSaida);
        LerEscrever.ficheiroExiste(ficheiro);
        Formatter fout = new Formatter(ficheiro);

        //Matriz que contém na posição [0][][] a matriz na comparação de critérios e nas restantes as matrizes de comparação de opções 
        double matrizes[][][] = new double[MAX_CRITERIOS][MAX_CRITERIOS][MAX_CRITERIOS];
        double matrizesNormalizadas[][][] = new double[MAX_CRITERIOS][MAX_CRITERIOS][MAX_CRITERIOS];
        double vetores_proprios[][] = new double[MAX_CRITERIOS][MAX_CRITERIOS];
        //Matriz guarda os cabeçalhos
        String cabecalhos[][] = new String[MAX_CRITERIOS][MAX_CRITERIOS];
        //inicia as variaveis
        int n_criterios = 0;
        int n_opções = 0;
        //Pergunta ao utilizador se quer calcular usando metodos de calculo de vetores proprios aproximados ou exato
        Scanner input = new Scanner(System.in);
        int metodo;
       
        //Lê matrizes de entrada
        try {
            int n[] = LerEscrever.lerFicheiro(ficheiroEntrada, matrizes, n_criterios, n_opções, cabecalhos);
            n_criterios = n[0];
            n_opções = n[1];
        } catch (FileNotFoundException e) {
            //mensagem de erro caso o ficheiro não exista
            Erro.logErros_consola("Erro: ficheiro não encontrado ");
            System.exit(0);
        }
        
        //Escolha do metodo
        do {
            System.out.print("Selecione a opção:\n"
                    + "(1) metodo exato (La4j)\n"
                    + "(2) metodo aproximado\n");
            metodo = input.nextInt();
        } while (metodo != 1 && metodo != 2);
        //Se L verifica se algum peso é inferior ao definido pelo utilizador
        if (l > -1) {
            String cabecalos_apagados[] = new String[n_criterios];
            Matematica.normalizar(matrizes, matrizesNormalizadas, n_criterios, n_opções);
            pesoCriterios(metodo, vetores_proprios, matrizes, n_criterios, matrizesNormalizadas);
            int posicoes[] = new int[n_criterios];
            int n_criterios_abaixo_L = Matematica.posicoesInferioresL(vetores_proprios[0], l, n_criterios, posicoes);
            n_criterios = Matematica.apagarMatrizes(cabecalos_apagados, matrizes, cabecalhos, n_criterios, n_opções, posicoes, n_criterios_abaixo_L);
            LerEscrever.printCabecalhosApagados(cabecalos_apagados, n_criterios_abaixo_L);
        }
        //Imprime as matrizes de entrada
        LerEscrever.divisoria("Dados de Entrada", fout);
        LerEscrever.print(matrizes, cabecalhos, n_criterios, n_opções, fout);
        //normaliza as matrizes
        Matematica.normalizar(matrizes, matrizesNormalizadas, n_criterios, n_opções);
        //Imprime as matrizes normalizadas
        LerEscrever.divisoriaSoFicheiro("Matrizes Normalizadas", fout);
        LerEscrever.printSoEcra(matrizesNormalizadas, cabecalhos, n_criterios, n_opções, fout);
        //Calculo e impressão de vetores Proprios
        vetoresProprios(fout, n_criterios, n_opções, metodo, matrizes, matrizesNormalizadas, vetores_proprios, cabecalhos);

        //Calculo do RC
        rc(fout, n_criterios, n_opções, matrizes, vetores_proprios, cabecalhos, s);

        //Calculo e impressão da prioridade composta
        prioridadeComposta(fout, vetores_proprios, n_criterios, n_opções, cabecalhos);

        //fecha ficheiro
        fout.close();

    }

    private static void pesoCriterios(int metodo, double[][] vetores_proprios, double[][][] matrizes, int n_criterios, double[][][] matrizesNormalizadas) {
        if (metodo == 1) {
            vetores_proprios[0] = Matematica.vetorPrioridadesExato(matrizes[0], n_criterios);
        } else {
            vetores_proprios[0] = Matematica.vetorPrioridadeAproximado(matrizesNormalizadas[0], n_criterios);
        }
    }

    private static void vetoresProprios(Formatter fout, int n_criterios, int n_opções, int metodo, double[][][] matrizes, double[][][] matrizesNormalizadas, double[][] vetores_proprios, String[][] cabecalhos) {
        LerEscrever.divisoriaSoFicheiro("Vetores Prórios", fout);
        for (int i = 0; i < n_criterios + 1; i++) {
            if (i == 0) {
                fout.format(cabecalhos[i][0] + "\n");
                //vetores proprios da matriz criterios
                pesoCriterios(metodo, vetores_proprios, matrizes, n_criterios, matrizesNormalizadas);

                for (int j = 0; j < n_criterios; j++) {
                    fout.format(Matematica.arredondar(vetores_proprios[i][j]) + "\n");
                }

                fout.format("\n");
            } else {
                fout.format(cabecalhos[i][0] + "\n");
                if (metodo == 1) {
                    vetores_proprios[i] = Matematica.vetorPrioridadesExato(matrizes[i], n_opções);
                } else {
                    vetores_proprios[i] = Matematica.vetorPrioridadeAproximado(matrizesNormalizadas[i], n_opções);
                }
                for (int j = 0; j < n_opções; j++) {
                    fout.format(Matematica.arredondar(vetores_proprios[i][j]) + "\n");
                }
                fout.format("\n");
            }
        }
    }

    private static void rc(Formatter fout, int n_criterios, int n_opções, double[][][] matrizes, double[][] vetores_proprios, String[][] cabecalhos, double s)throws IOException{
        LerEscrever.divisoriaSoFicheiro("RC", fout);
        for (int i = 0; i < n_criterios + 1; i++) {
            int n = n_opções;
            if (i == 0) {
                n = n_criterios;
            }
            double[][] tmp = new double[n][n];
            for (int j = 0; j < n; j++) {
                for (int w = 0; w < n; w++) {
                    tmp[j][w] = matrizes[i][j][w];
                }
            }
            fout.format("\n" + cabecalhos[i][0] + "\n");

            double rc = Matematica.calculoRC(tmp, Arrays.copyOf(vetores_proprios[i], n), n, fout);
            LerEscrever.printRc(rc, s, fout);
        }
    }

    private static void prioridadeComposta(Formatter fout, double[][] vetores_proprios, int n_criterios, int n_opções, String[][] cabecalhos) {
 
        fout.format("\n");
        LerEscrever.divisoria("Prioridade Composta", fout);
        
        fout.format("\n");
        double[] prioridadecomposta = Matematica.prioridadeComposta(vetores_proprios, n_criterios, n_opções);

        for (int i = 0; i < n_opções; i++) {
            System.out.println(Matematica.arredondar(prioridadecomposta[i]));
            fout.format(Matematica.arredondar(prioridadecomposta[i]) + "\n");
        }
        //Escolha da melhor alternativa
        int pos = Matematica.escolherOpção(prioridadecomposta);
        
        LerEscrever.divisoria("Melhor opção", fout);
        System.out.println( cabecalhos[1][pos + 1] + "\n");
        fout.format("\n" + cabecalhos[1][pos + 1] + "\n");

    }

    
}
