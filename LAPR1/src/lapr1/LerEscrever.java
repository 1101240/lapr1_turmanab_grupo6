/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Formatter;

public class LerEscrever {

    /**
     *
     * @param nomeFicheiro
     * @param matrizes
     * @param n_criterios
     * @param n_opcoes
     * @param cabecalhos
     * @return
     * @throws FileNotFoundException
     */
    public static int[] lerFicheiro(String nomeFicheiro, double[][][] matrizes, int n_criterios, int n_opcoes, String[][] cabecalhos) throws IOException {
        File ficheiro = new File(nomeFicheiro);
        Scanner flinput = new Scanner(ficheiro);

        int[] n = new int[2];

        int n_matrizes = 1;
        int n_linha = 0;
        boolean contador = false;
        //Lê linhas do finheiro enquanto o ficheiro não acabar e o numero de linhas for inferior ao MAX_CRITERIOS
        while (flinput.hasNext() && n_criterios < Ahp.MAX_CRITERIOS) {
            String linha = flinput.nextLine();
            if (linha.length() > 0) { //se a linha não for 0 vai guardar a informação
                contador = true;
                if (n_matrizes == 1 && n_linha == 0) {
                    n[0] = encontrarN(linha); //encontra n_criterios
                    n_criterios = n[0];
                    if (n[0] < 1) {
                        Erro.logErros_consola("Ficheiro de leitura invalido - numero de critérios inválidos");

                    }

                }
                if (n_matrizes == 2 && n_linha == 0) {
                    n[1] = encontrarN(linha); //encontra n_opções
                    n_opcoes = n[1];
                    if (n[1] < 1) {
                        Erro.logErros_consola("Ficheiro de leitura invalido - numero de opções inválidas");

                    }
                }

                guardarDados(linha, matrizes, n_criterios, n_matrizes, n_linha, cabecalhos, n_opcoes);
                n_linha++;
            } else {
                if (contador == true) {

                    if (n_linha != n_criterios + 1 && n_matrizes == 1) {
                        Erro.logErros_consola("Matriz criterios não tem o numero de linhas certo");
                    } else if (n_linha != n_opcoes + 1 && n_matrizes > 1) {
                        Erro.logErros_consola("Matriz  não tem o numero de linhas certo");
                    }
                    
                    n_matrizes++;

                    n_linha = 0;
                    contador = false;
                }
            }

        }

        if (n_matrizes != n_criterios + 1) {
            Erro.logErros_consola("Numero de matrizes invalido");
        }
        if (n[0] == 0 || n[1] == 0) {
            Erro.logErros_consola("Ficheiro invalido");
        }
        
        return n;

    }

    /**
     * s
     *
     * @param linha
     * @param matrizes
     * @param n_criterios
     * @param n_matrizes
     * @param n_linha
     * @param cabecalhos
     */
    public static void guardarDados(String linha, double[][][] matrizes, int n_criterios, int n_matrizes, int n_linha, String[][] cabecalhos, int n_opcoes) throws IOException {
        if (!linha.trim().substring(0, 1).matches("\\s*[0-9m]")) {
            Erro.logErros_consola("Ficheiro invalido - Linha invalida");
        }
        if (linha.charAt(0) == 'm' && linha.charAt(1) == 'c') {
            String temp[] = linha.split("  ");
            for (int i = 0; i < temp.length; i++) {
                cabecalhos[n_matrizes - 1][i] = temp[i];
            }

        } else {
            String temp[] = linha.trim().replaceAll("\\s+", " ").split(" ");
            if (n_matrizes == 1 && temp.length != n_criterios) {
                Erro.logErros_consola("Matriz critérios está mal preenchida");
            } else if (n_matrizes > 1 && temp.length != n_opcoes) {
                Erro.logErros_consola("Matriz de comparação está mal preenchida");
            }
            for (int i = 0; i < temp.length; i++) {
                if (!temp[i].matches("[0-9/.]*")) {
                    Erro.logErros_consola("Matrizes contem caracteres invalidos");
                }
                if (temp[i].contains("/")) {
                    String temporaria[] = temp[i].split("/");
                    matrizes[n_matrizes - 1][n_linha - 1][i] = Double.parseDouble(temporaria[0]) / Double.parseDouble(temporaria[1]);
                } else {
                    matrizes[n_matrizes - 1][n_linha - 1][i] = Double.parseDouble(temp[i]);
                }
            }
        }
    }

    public static int[] lerTopsis(String nomeFicheiro, String[] crt_beneficio, String[] crt_custo, String[] crt, String[] alt, String[] vec_pesos_cabecalho, double[] vec_pesos, double[][] matriz) throws IOException {
        File ficheiro = new File(nomeFicheiro);
        Scanner flinput = new Scanner(new File(nomeFicheiro));

        //Guarda o tamanho dos vetore
        int[] n = new int[4];
        int estado = 0;
        int n_linha = 0;
        while (flinput.hasNext()) {
            String linha = flinput.nextLine();
            if (linha.length() > 0) {
                if (estado == 0) {
                    estado = guardarTopsis(linha, crt_beneficio, crt_custo, crt, alt, vec_pesos_cabecalho, n);
                } else if (estado == 1) {
                    guardarvecPesos(linha, vec_pesos);
                    estado = 0;
                } else if (estado == 2) {
                    guardarMatriz(linha, n_linha, matriz, n[2], n[3]);
                    n_linha++;
                }
            }
        }
        if (n_linha != n[3]) {
            Erro.logErros_consola("Matriz mal preenchida");
        }
        if (vec_pesos[n[2] - 1] == 0 || vec_pesos[n[2]] != 0) {
            Erro.logErros_consola("Vector pesos mal preenchido");
        }
        
        return n;
    }

    private static void guardarMatriz(String linha, int n_linha, double[][] matriz, int crt, int alt) throws IOException {
        String temp[] = linha.trim().replaceAll("\\s+", " ").split(" ");
        if (temp.length != crt || n_linha + 1 > alt) {
            Erro.logErros_consola("Matriz mal preenchida");
        }
        for (int i = 0; i < temp.length; i++) {
            if (!temp[i].matches("[0-9]*")) {
                Erro.logErros_consola("Matriz contem caracteres invalidos");
            }
            matriz[i][n_linha] = Integer.parseInt(temp[i]);
        }
    }

    private static void guardarvecPesos(String linha, double[] vec_pesos) throws IOException {
        String temp[] = linha.trim().replaceAll("\\s+", " ").split(" ");

        for (int i = 0; i < temp.length; i++) {
            if (!temp[i].matches("[0-9.]*")) {
                Erro.logErros_consola("Vetor pesos contem caracteres invalidos");
            }
            vec_pesos[i] = Double.parseDouble(temp[i]);
        }
    }

    private static int guardarTopsis(String linha, String[] crt_beneficio, String[] crt_custo, String[] crt, String[] alt, String[] vec_pesos_cabecalho, int[] n) throws IOException {
        if (Character.isLetter(linha.charAt(0))) {
            String temp[] = linha.split("  ");
            if (temp[0].equals("crt_beneficio")) {
                guardarCriterios(crt_beneficio, temp);
                n[0] = temp.length - 1;
                return 0;
            } else if (temp[0].equals("crt_custo")) {
                guardarCriterios(crt_custo, temp);
                n[1] = temp.length - 1;
                return 0;
            } else if (temp[0].equals("crt")) {
                guardarCriterios(crt, temp);
                n[2] = temp.length - 1;
                return 0;
            } else if (temp[0].equals("alt")) {
                guardarCriterios(alt, temp);
                n[3] = temp.length - 1;
                return 2;
            } else if (temp[0].equals("vec_pesos")) {
                guardarCriterios(vec_pesos_cabecalho, temp);
                return 1;
            }

        } else {
            Erro.logErros_consola("Ficheiro invalido");
        }
        return 0;
    }

    public static void guardarCriterios(String[] criterios, String[] temp) {
        for (int i = 1; i < temp.length; i++) {
            criterios[i - 1] = temp[i];
        }
    }

    /**
     *
     * @param linha
     * @return
     */
    public static int encontrarN(String linha) {
        String temp[] = linha.trim().replaceAll("\\s+", " ").split(" ");
        int n = temp.length - 1;

        return n;
    }

    /**
     *
     * @param matrizes
     * @param cabecalhos
     * @param n_criterios
     * @param n_opções
     */
    public static void print(double[][][] matrizes, String[][] cabecalhos, int n_criterios, int n_opções, Formatter fout) {
        for (int i = 0; i < n_criterios + 1; i++) {
            if (i == 0) {
                System.out.println(cabecalhos[0][i]);
                fout.format(cabecalhos[0][i] + "\n");

                for (int j = 0; j < n_criterios; j++) {
                    for (int w = 0; w < n_criterios; w++) {
                        System.out.print(Matematica.arredondar(matrizes[i][j][w]) + "\t");
                        fout.format(Matematica.arredondar(matrizes[i][j][w]) + "\t");
                    }
                    System.out.println();
                    fout.format("\n");
                }
                System.out.println("\n\n");
                fout.format("\n\n\n");
            } else {
                System.out.println(cabecalhos[0][i]);
                fout.format(cabecalhos[0][i] + "\n");
                for (int j = 0; j < n_opções; j++) {

                    for (int w = 0; w < n_opções; w++) {
                        System.out.print(Matematica.arredondar(matrizes[i][j][w]) + " \t");
                        fout.format(Matematica.arredondar(matrizes[i][j][w]) + " \t");
                    }
                    System.out.println();
                    fout.format("\n");
                }
                System.out.println("\n\n");
                fout.format("\n\n\n");
            }
        }
    }

    /**
     *
     * @param matrizes
     * @param cabecalhos
     * @param n_criterios
     * @param n_opções
     */
    public static void printSoEcra(double[][][] matrizes, String[][] cabecalhos, int n_criterios, int n_opções, Formatter fout) {
        for (int i = 0; i < n_criterios + 1; i++) {
            if (i == 0) {
                fout.format(cabecalhos[0][i] + "\n");

                for (int j = 0; j < n_criterios; j++) {
                    for (int w = 0; w < n_criterios; w++) {
                        fout.format(Matematica.arredondar(matrizes[i][j][w]) + "\t");
                    }
                    fout.format("\n");
                }
                fout.format("\n\n\n");
            } else {
                fout.format(cabecalhos[0][i] + "\n");
                for (int j = 0; j < n_opções; j++) {

                    for (int w = 0; w < n_opções; w++) {
                        fout.format(Matematica.arredondar(matrizes[i][j][w]) + " \t");
                    }
                    fout.format("\n");
                }
                fout.format("\n\n\n");
            }
        }
    }

    /**
     *
     * @param titulo
     */
    public static void divisoria(String titulo, Formatter fout) {
        String[] chars = new String[titulo.length() + 4];
        Arrays.fill(chars, "*");
        for (String x : chars) {
            fout.format(x);
            System.out.print(x);
        }
        System.out.println();
        fout.format("\n");
        System.out.println("* " + titulo + " *");
        fout.format("* " + titulo + " *" + "\n");

        for (String x : chars) {
            fout.format(x);
            System.out.print(x);
        }
        System.out.println();
        fout.format("\n");

    }

    public static void divisoriaSoFicheiro(String titulo, Formatter fout) {
        String[] chars = new String[titulo.length() + 4];
        Arrays.fill(chars, "*");
        for (String x : chars) {
            fout.format(x);
        }
        fout.format("\n");
        fout.format("* " + titulo + " *" + "\n");

        for (String x : chars) {
            fout.format(x);
        }
        fout.format("\n");

    }

    public static void printCabecalhosApagados(String[] cabecalhosApagados, int n) {
        System.out.println("Os seguintes critérios não cumprem os requesitos:");
        for (int i = 0; i < n; i++) {
            System.out.println("-" + cabecalhosApagados[i]);
        }
    }

    public static void printRc(double rc, double s, Formatter fout) throws IOException {
        if (rc == 0) {
            fout.format("Erro, dividir por 0 \n");
        } else {
            fout.format("RC = " + rc + "\n");

            if (s > -1) {
                if (rc < s) {

                    fout.format("O valor do RC é inferior a " + s + "\n");

                } else {

                    Erro.logErros_consola("O valor do rc é superior ao definido pelo utilizador \n");
                }
            } else if (rc < 0.1) {
                fout.format("O valor de RC é inferior a 0.1 \n");
            } else {
                fout.format("O valor de RC é superior a 0.1 \n");
            }

        }
    }

    public static void imprimirVetores(String[] vetor, Formatter fout, int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(vetor[i] + "\t");
            fout.format(vetor[i] + "\n");
        }
        System.out.println();
        fout.format("\n");
    }

    public static void imprimirVetoresInt(double[] vetor, Formatter fout, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(Matematica.arredondar(vetor[i]) + "\t");
            fout.format(Matematica.arredondar(vetor[i]) + "\n");
        }
        System.out.println();
        fout.format("\n");
    }

    public static void imprimirVetoresIntSoficheiro(double[] vetor, Formatter fout, int n) {
        for (int i = 0; i < n; i++) {
            fout.format(Matematica.arredondar(vetor[i]) + "\n");
        }
        fout.format("\n");
    }

    public static void printMatriz(String[] alt, String[] crit, double[][] matriz, int n_alt, int n_crit, Formatter fout) {
        System.out.print("\t");
        fout.format("\t");
        for (int i = 0; i < n_crit; i++) {
            System.out.print(crit[i] + "\t");
            fout.format(crit[i] + "\t");
        }
        System.out.println();
        fout.format("\n");

        for (int i = 0; i < n_crit; i++) {
            System.out.print(alt[i] + "\t");
            fout.format(alt[i] + "\t");
            for (int j = 0; j < n_alt; j++) {
                System.out.printf(Matematica.arredondar(matriz[j][i]) + "\t");
                fout.format(Matematica.arredondar(matriz[j][i]) + "\t");
            }
            System.out.println();
            fout.format("\n");
        }

    }

    public static void printMatrizSoficheiro(String[] alt, String[] crit, double[][] matriz, int n_alt, int n_crit, Formatter fout) {

        fout.format("\t");
        for (int i = 0; i < n_crit; i++) {
            fout.format(crit[i] + "\t");
        }

        fout.format("\n");

        for (int i = 0; i < n_crit; i++) {
            fout.format(alt[i] + "\t");
            for (int j = 0; j < n_alt; j++) {
                fout.format(Matematica.arredondar(matriz[j][i]) + "\t");
            }
            fout.format("\n");
        }

    }

    public static void imprimirVetoresLinha(String[] vetor, Formatter fout, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(vetor[i] + "\t");
            fout.format(vetor[i] + "\t");
        }
        System.out.println();
        fout.format("\n");
    }

    public static void ficheiroExiste(File ficheiro) {
        if (ficheiro.exists()) {
            System.out.print("O ficheiro já existe. Tem a certeza que quer escrever por cima?[s/n]");
            Scanner in = new Scanner(System.in);
            String opcao = "";
            do {
                opcao = in.next();
                if (opcao.equals("n")) {
                    System.exit(0);
                }
                
            } while (!opcao.equals("s"));
        }
    }

    

    
}
