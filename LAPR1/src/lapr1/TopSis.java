/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1;

import java.io.IOException;
import java.util.Formatter;
import java.io.File;


public class TopSis {

    public static void topsis(String nomeFicheiro, String ficheiroSaida) throws IOException {
        //Formmater para escrever num ficheiro indicado pelo utilizador ao chamar a app na linha de comando
        File ficheiro = new File(ficheiroSaida);
        LerEscrever.ficheiroExiste(ficheiro);
        Formatter fout = new Formatter(new File(ficheiroSaida));
        
        //Configuração do método
        final int maxCritAlt = 20;
        String[] crt_beneficio = new String[maxCritAlt];
        String[] crt_custo = new String[maxCritAlt];
        String[] crt = new String[maxCritAlt];
        String[] alt = new String[maxCritAlt];
        String[] vec_pesos_cabecalho = new String[maxCritAlt];
        double[] vec_pesos = new double[maxCritAlt];
        double[][] matriz = new double[maxCritAlt][maxCritAlt];
        
        //Lê ficheiro e devolve um vetor n com numero de criterio beneficio, criterio custo, criterios e alternativas
        int[] n = new int[4];
        n = LerEscrever.lerTopsis(nomeFicheiro, crt_beneficio, crt_custo, crt, alt, vec_pesos_cabecalho, vec_pesos, matriz);
        int n_crt_beneficios = n[0];
        int n_crt_custo = n[1];
        int n_crt = n[2];
        int n_alt = n[3];
        
        //vetores
        double[] ideal = new double[n_crt];
        double[] idealneg = new double[n_crt];
        double[] vetorfinal = new double[n_crt];
        double[] solução = new double[n_crt];
        double[] soluçaoneg = new double[n_crt];
        //Matrizes
        double[][] normalizada = new double[n_alt][n_crt];
        double[][] normalizadaPesada = new double[n_alt][n_crt];
        double[][] matrizdistancia = new double[n_alt][n_crt];
        double[][] matrizdistancianeg = new double[n_alt][n_crt];
        
        //Dados de Entrada
        LerEscrever.divisoria("Beneficios", fout);
        if(n_crt_beneficios == 0){
            LerEscrever.imprimirVetores(crt, fout, n_crt);
        }else{
            LerEscrever.imprimirVetores(crt_beneficio, fout, n_crt_beneficios);
        }
        LerEscrever.divisoria("Custos", fout);
        LerEscrever.imprimirVetores(crt_custo, fout, n_crt_custo);
        LerEscrever.divisoria("Pesos", fout);
        LerEscrever.imprimirVetoresLinha(vec_pesos_cabecalho, fout, n_crt);
        LerEscrever.imprimirVetoresInt(vec_pesos, fout, n_crt);
        LerEscrever.divisoria("Matriz", fout);
        LerEscrever.printMatriz(alt, crt, matriz, n_alt, n_crt, fout);
        //Matriz normalizada
        normalizada = Matematica.normalizarTOPSIS(matriz, n_alt, n_crt);
        LerEscrever.divisoriaSoFicheiro("Normalizada", fout);
        LerEscrever.printMatrizSoficheiro(alt, crt, normalizada, n_alt, n_crt, fout);
        //Matriz normalizada pesada
        LerEscrever.divisoria("Normalizada pesada", fout);
        normalizadaPesada = Matematica.normalizapesada(normalizada, vec_pesos, n_crt, n_alt);
        LerEscrever.printMatriz(alt, crt, normalizadaPesada, n_alt, n_crt, fout);
        //Vetor ideal
        procurar(crt, crt_custo, n_crt, n_crt_custo, ideal, idealneg, normalizadaPesada, n_alt);
        LerEscrever.divisoriaSoFicheiro("Vetor ideal", fout);
        LerEscrever.imprimirVetoresIntSoficheiro(ideal, fout, n_crt);
        //vetor ideal negativo
        LerEscrever.divisoriaSoFicheiro("Vetor ideal negativo", fout);
        LerEscrever.imprimirVetoresIntSoficheiro(idealneg, fout, n_crt);   
        //Matriz distancia
        LerEscrever.divisoriaSoFicheiro("Matriz distancia", fout);
        matrizdistancia = Matematica.matrizDistancia(normalizadaPesada, ideal, n_alt, n_crt);
        LerEscrever.printMatrizSoficheiro(alt, crt, matrizdistancia, n_alt, n_crt, fout);
        //Matriz distancia negativa
        LerEscrever.divisoriaSoFicheiro("Matriz distancia negativa", fout);
        matrizdistancianeg = Matematica.matrizDistancia(normalizadaPesada, idealneg, n_alt, n_crt);
        LerEscrever.printMatrizSoficheiro(alt, crt, matrizdistancianeg, n_alt, n_crt, fout);
        //Vetor solução
        LerEscrever.divisoriaSoFicheiro("vetor solução", fout);
        solução = Matematica.vetorSolucao(matrizdistancia, n_alt);
        LerEscrever.imprimirVetoresIntSoficheiro(solução, fout, n_crt);
        //Vetor solução negatova
        LerEscrever.divisoriaSoFicheiro("vetor soluçao negativa", fout);
        soluçaoneg = Matematica.vetorSolucao(matrizdistancianeg, n_alt);      
        LerEscrever.imprimirVetoresIntSoficheiro(soluçaoneg, fout, n_crt);
       //vetor final
        LerEscrever.divisoria("Vetor distancia", fout);
        vetorfinal = Matematica.vetorFinal(solução, soluçaoneg, n_alt);
        LerEscrever.imprimirVetoresInt(vetorfinal, fout, n_crt);

        //Melhor opção
        LerEscrever.divisoria("Melhor opção", fout);
        int pos = Matematica.escolherOpção(vetorfinal);
        System.out.println(alt[pos]);
        fout.format(alt[pos]);
        
        
        //fecha ficheiro
        fout.close();
        
    }
        /**
         * 
         * @param custos - critérios de custo;
         * @param criterios - todos os critérios;
         * @param n - número de critérios;
         * @return - se o critério é custo;
         */
     private static boolean custo(String[] custos, String criterios, int n) {
        for (int i = 0; i < n; i++) {

            if (custos[i].trim().equals(criterios.trim())) {
                return true;

            }

        }
        return false;

    }
     /**
      * 
      * @param criterios - vetor com todos os critérios;
      * @param custos - vetor com critérios de custo;
      * @param ncriterios - número de critérios;
      * @param ncustos - número de critérios de custo;
      * @param ideal - vetor com maiores benefícios e menores custos;
      * @param idealneg - vetor com piores benefícios e maiores custos;
      * @param matriz - matriz normalizada pesada;
      * @param nopcoes - número de opções
      */
    public static void procurar(String[] criterios, String[] custos, int ncriterios, int ncustos, double[] ideal, double[] idealneg, double[][] matriz, int nopcoes) {
        for (int j = 0; j < ncriterios; j++) {

            if (custo(custos, criterios[j], ncustos)) {
                ideal[j] = procurarMenor(matriz, nopcoes, j);
                idealneg[j] = procurarMaior(matriz,  nopcoes,j);
            } else {
                idealneg[j] = procurarMenor(matriz, nopcoes, j);
                ideal[j] = procurarMaior(matriz,  nopcoes,j);
            }

        }

    }

    /**
     * @param matriz - matriz normalizada pesada;
     * @param N_CRITERIOS - número de critérios;
     * @param posicao - coluna da matriz;
     * @return - maior valor da coluna;
     */
    public static double procurarMaior (double [][] matriz, int N_CRITERIOS, int posicao){
        double maior = 0;
        
        for (int i = 0; i < N_CRITERIOS; i++){
            if(matriz[posicao][i] > maior){
                maior = matriz[posicao][i];
            }
        }
        return maior;
    }
    /**
     * @param matriz - matriz normalizada pesada;
     * @param N_CRITERIOS - número de critérios;
     * @param posicao - coluna da matriz;
     * @return - menor valor da coluna;
     */
    public static double procurarMenor (double [][] matriz, int N_CRITERIOS, int posicao){
        double menor = 10;
        
        for (int i = 0; i < N_CRITERIOS; i++){
            if(matriz[posicao][i] < menor){
                menor = matriz[posicao][i];
            }
        }
        return menor;
    }

}
