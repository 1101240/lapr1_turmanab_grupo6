/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1;

import java.io.IOException;

public class Lapr1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        if (args.length < 4) {
            Erro.consola("Argumentos em falta");
        }else if(args.length == 5 || args.length == 7){
            Erro.consola("Numero de argumentos invalidos");
        }
        
        //Metodo Ahp
        if (args[0].equals("-M") && args[1].equals("1")) {
            double l = -1;
            double s = -1;
            if (args.length > 4) {
                if (args[2].equals("-L")) {
                    try{
                    if (Double.parseDouble(args[3]) < 0) {
                        Erro.consola("L não pode ser negativo");
                    }
                    } catch (NumberFormatException e){
                        Erro.consola("ERRO:String em vez de double");
                    }
                    l = Double.parseDouble(args[3]);
                } else if (args[2].equals("-S")) {
                    try{
                    if (Double.parseDouble(args[3]) < 0) {
                        Erro.consola("S não pode ser negativo");
                    }
                    } catch (NumberFormatException e){
                        Erro.consola("ERRO: String em vez de double");
                    }
                    s = Double.parseDouble(args[3]);
                }
            } else if (args.length > 6) {
                if (args[4].equals("-L")) {
                    try{
                    if (Double.parseDouble(args[5]) < 0) {
                        Erro.consola("L não pode ser negativo");
                    }
                    }catch (NumberFormatException e) {
                        Erro.consola("ERRO: String em vez de double "  );
                    }
                    
                    l = Double.parseDouble(args[5]);
                } else if (args[4].equals("-S")) {
                    if (Integer.parseInt(args[5]) < 0) {
                        Erro.consola("S não pode ser negativo");
                    }
                    s = Double.parseDouble(args[5]);
                }
            }

            Ahp.ahp(l, s, args[args.length - 2], args[args.length - 1]);
        //Metodo Topsis
        } else if (args[0].equals("-M") && args[1].equals("2")) {
            if(args.length != 4 ){
                Erro.consola("Argumentos Invalidos");
            }
            TopSis.topsis(args[2], args[3]);

        } else {
            Erro.consola("Argumentos inválidos");
        }
    }
}
