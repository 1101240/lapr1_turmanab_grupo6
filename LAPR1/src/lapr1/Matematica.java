/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1;

import org.la4j.Matrix;
import org.la4j.Vector;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.vector.dense.BasicVector;
import java.util.Formatter;

/**
 * Classe em que se encontram todos os métodos associados a operações
 * matemáticas
 *
 */
public class Matematica {

    //
    /**
     * Metodo que normaliza uma matriz
     *
     * @param matrizes
     * @param matrizesNormalizadas
     * @param n_criterios
     * @param n_opcoes
     */
    public static void normalizar(double[][][] matrizes, double[][][] matrizesNormalizadas, int n_criterios, int n_opcoes) {

        double[] somaCri = new double[n_criterios];

        //entra no ciclo apenas se for a primeira matriz ( matriz de criterios )
        for (int i = 0; i < n_criterios + 1; i++) {
            if (i == 0) {
                //cria vetor soma das colunas para a matriz de criterios
                for (int w = 0; w < n_criterios; w++) {
                    for (int j = 0; j < n_criterios; j++) {
                        somaCri[w] += matrizes[i][j][w];

                    }
                }
                //calcula matriz normalizada de critérios
                for (int w = 0; w < n_criterios; w++) {
                    for (int j = 0; j < n_criterios; j++) {
                        matrizesNormalizadas[i][j][w] = matrizes[i][j][w] / somaCri[w];

                    }
                }

            } else {
                double[] somaOp = new double[n_opcoes];
                //cria vetor soma das colunas para as matrizes das opções
                for (int w = 0; w < n_opcoes; w++) {
                    for (int j = 0; j < n_opcoes; j++) {
                        somaOp[w] += matrizes[i][j][w];

                    }
                }
                //calcula as matrizes normalizadas de opções
                for (int w = 0; w < n_opcoes; w++) {
                    for (int j = 0; j < n_opcoes; j++) {
                        matrizesNormalizadas[i][j][w] = matrizes[i][j][w] / somaOp[w];
                    }
                }

            }

        }

    }

    /**
     * calcula o vetor prioridade Exato
     *
     * @param matriz
     * @param n
     * @return
     */
    public static double[] vetorPrioridadesExato(double[][] matriz, int n) {
        int posicao = 0;
        double[] vetor_posicao = new double[n];
        // Criar objeto do tipo Matriz
        Matrix a = new Basic2DMatrix(matriz);

        //Obtem valores e vetores próprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

        // converte objeto Matrix (duas matrizes)  para array Java        
        double matA[][] = mattD[0].toDenseMatrix().toArray();

        double matB[][] = mattD[1].toDenseMatrix().toArray();

        double max = mattD[1].max();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matB[i][j] == max) {
                    posicao = j;
                }
            }
        }

        double k = mattD[0].getColumn(posicao).sum();

        for (int i = 0; i < n; i++) {
            vetor_posicao[i] = (matA[i][posicao] / k);
        }
        return vetor_posicao;
    }

    /**
     * Calculo do vetor_posiçao aproximado
     *
     * @param matrizNormalizada
     * @param n ordem da matriz
     * @return vetor_posição
     */
    public static double[] vetorPrioridadeAproximado(double[][] matrizNormalizada, int n) {
        double[] vetor_posicao = new double[n];
        double soma = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                soma = soma + matrizNormalizada[i][j];
            }
            vetor_posicao[i] = soma / n;
            soma = 0;
        }

        return vetor_posicao;
    }

    /**
     * Calcula A x X para o calculo do lambda
     *
     * @param a
     * @param vPrioridade
     * @return
     */
    public static double[] aX(double[][] a, double[] vPrioridade) {

        /*para teste
        double b[][] = {{1, 0.5, 3}, {2, 1, 4}, {0.3333, 0.25, 1}};
        double vPrioridad[] = {0.32, 0.56, 0.12};
         */
        // Criar objeto do tipo Matriz
        Matrix a_matrix = new Basic2DMatrix(a);
        Vector vetor_prioridade = new BasicVector(vPrioridade);

        Vector C = a_matrix.multiply(vetor_prioridade);

        double[] aX = C.toDenseVector().toArray();
        /* Para testes
        for (int i = 0; i < 3; i ++){
            System.out.println(aX[i]);
        } |*/

        return aX;
    }

    /**
     * Calcula o lambda máximo.
     *
     * @param matrizes
     * @param vetorprioridade
     * @param n ordem da matriz
     * @return lambda máximo
     */
    public static double calculoYmax(double[][] matrizes, double[] vetorprioridade, int n, Formatter fout) {
        double[] ax = aX(matrizes, vetorprioridade);
        double ymax = 0;
        double soma = 0;
        //O ciclo for divide cada valor na posição i do vetor aX pela respetiva
        for (int i = 0; i < n; i++) {
            soma = soma + (ax[i] / vetorprioridade[i]);
        }
        // posição no vetor prioridade, utilizando uma variável de soma, sendo esta variável dividida por n.
        ymax = soma / n;
        fout.format("Ymax: " + ymax + "\n");
        return ymax;
    }

    /**
     * Método para calcular o Índice de Consistência, utilizando a fórmula
     * |(ymax-n)/(n-1)|
     *
     * @param matrizes
     * @param vetorprioridade
     * @param n Ordem da matriz
     * @return Valor do Índice de Consistência
     */
    public static double calculoIC(double[][] matrizes, double[] vetorprioridade, int n, Formatter fout) {
        double ymax = calculoYmax(matrizes, vetorprioridade, n, fout);
        double IC = 0;
        IC = Math.abs((ymax - n) / (n - 1));
        return IC;
    }

    /**
     * Calculo do RC
     *
     * @param matrizes
     * @param vetorprioridade
     * @param n - Ordem da Matriz
     * @return
     */
    public static double calculoRC(double[][] matrizes, double[] vetorprioridade, int n, Formatter fout) {
        double IC = calculoIC(matrizes, vetorprioridade, n, fout);
        double[] ir = new double[]{0, 0, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41};
        double RC;

        if (n <= 2) {
            RC = 0;
        } else {
            RC = IC / ir[n - 1];
            fout.format("IR: "+ ir[n-1] + "\n");
        }
        return arredondar(RC);
    }

    /**
     * Calculo prioridade composta
     *
     * @param vetores_proprios
     * @param n_criterios
     * @param n_opcoes
     * @return
     */
    public static double[] prioridadeComposta(double[][] vetores_proprios, int n_criterios, int n_opcoes) {
        double[][] tmp = new double[n_opcoes][n_criterios];
        //Passar a matriz vetores proprios para uma tmp com a dimensão correta para passar para matriz
        for (int i = 0; i < n_opcoes; i++) {
            for (int j = 1; j < n_criterios + 1; j++) {
                tmp[i][j - 1] = vetores_proprios[j][i];

            }
        }

        return multiplicaçao(tmp, vetores_proprios[0], n_opcoes, n_criterios);
    }

    /**
     * Faz a multiplicação de uma matriz por um vetor
     *
     * @param matriz
     * @param vetor
     * @param n_opcoes
     * @param n_criterios
     * @return Um vetor com o resultado da multiplicação
     */
    public static double[] multiplicaçao(double[][] matriz, double[] vetor, int n_opcoes, int n_criterios) {
        double[] multiplicacao = new double[n_opcoes];
        //inicia o array com todos os valores a 0
        for (int i = 0; i < multiplicacao.length; i++) {
            multiplicacao[i] = 0;
        }
        for (int i = 0; i < n_opcoes; i++) {
            for (int j = 0; j < n_criterios; j++) {
                multiplicacao[i] = multiplicacao[i] + (matriz[i][j] * vetor[j]);
            }
        }
        return multiplicacao;
    }

    /**
     * Procura o maior do array
     *
     * @param array
     * @return Posição do valor maior do array
     */
    public static int escolherOpção(double[] array) {
        double maior = 0;
        int pos = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maior) {
                maior = array[i];
                pos = i;
            }
        }

        return pos;
    }

    /**
     * Arredonda um numero
     *
     * @param numero
     * @return Numero arredondado para 3 casas decimeis
     */
    public static double arredondar(double numero) {
        double arredondado;
        arredondado = (double) Math.round(numero * 1000) / 1000;

        return arredondado;
    }

    /**
     *
     * @param vetores_proprios
     * @param L
     * @param n_criterios
     * @param posicoes
     * @return
     */
    public static int posicoesInferioresL(double vetores_proprios[], double L, int n_criterios, int posicoes[]) {
        int n = 0;
        for (int i = 0; i < n_criterios; i++) {
            if (vetores_proprios[i] < L) {
                posicoes[n] = i;
                n++;
            }
        }
        return n;
    }

    public static int apagarMatrizes(String[] cabecalos_apagados, double matrizes[][][], String[][] cabecalhos, int n_criterios, int n_opções, int posicoes[], int n_criterios_abaixo_L) {
        int n_apagados = 0;

        for (int n = 0; n < n_criterios_abaixo_L; n++) {
            //apaga coluna
            for (int i = 0; i < n_criterios; i++) {
                if (i == posicoes[n] - n_apagados) {
                    cabecalos_apagados[n_apagados] = cabecalhos[0][i];
                    for (int w = i; w < n_criterios; w++) {

                        cabecalhos[0][w] = cabecalhos[0][w + 1];
                    }

                    for (int linha = i; linha < n_criterios; linha++) {
                        for (int j = 0; j < n_criterios; j++) {
                            matrizes[0][linha][j] = matrizes[0][linha][j + 1];

                        }
                    }

                }
            }
            //apaga coluna
            for (int i = 0; i < n_criterios; i++) {
                if (i == posicoes[n] - n_apagados) {
                    for (int coluna = i; coluna < n_criterios; coluna++) {
                        for (int j = 0; j < n_criterios; j++) {
                            matrizes[0][i][coluna] = matrizes[0][i + 1][coluna];

                        }
                    }

                }
            }
            n_apagados = n_apagados + 1;

        }
        n_apagados = 0;
        for (int n = 0; n < n_criterios_abaixo_L; n++) {
            for (int n_matrizes = 1; n_matrizes < n_criterios + 1; n_matrizes++) {
                if (n_matrizes == posicoes[n] - n_apagados) {
                    matrizes[n_matrizes] = matrizes[n_matrizes + 1];
                }
            }
            n_apagados = n_apagados + 1;
        }

        n_criterios = n_criterios - n_criterios_abaixo_L;
        return n_criterios;
    }

    
    
    //TOPSIS
    
    /**
     * Método para calculo da matriz normalizada.
     *
     * @param matriz - matriz
     * @param n_opcoes - nº de opções do problema
     * @param n_criterios - nº de critérios do problema
     * @return normalizada - matriz normalizada
     */
    public static double[][] normalizarTOPSIS(double[][] matriz, int n_opcoes, int n_criterios) {
        double[][] normalizada = new double[n_criterios][n_opcoes];
        double[] quadsoma = new double[n_opcoes];
        double[] raizquadsoma = new double[n_criterios];
        //cria um vetor com a soma dos quadrados de cada critério
        for (int j = 0; j < n_criterios; j++) {
            for (int i = 0; i < n_opcoes; i++) {
                quadsoma[j] = quadsoma[j] + Math.pow(matriz[j][i], 2);
            }
        }
        //faz a raiz quadrada da soma anterior
        for (int i = 0; i < n_criterios; i++) {
            raizquadsoma[i] = Math.sqrt(quadsoma[i]);
        }
        //calcula a matriz normalizada 
        for (int j = 0; j < n_criterios; j++) {
            for (int i = 0; i < n_opcoes; i++) {
                normalizada[j][i] = matriz[j][i] / raizquadsoma[j];
            }
        }

        return normalizada;
    }

    /**
     * Método para calculo da matriz normalizada pesada.
     *
     * @param matriznormalizada - matriz normalizada
     * @param vetorPesos - vetor com o peso dado a cada critério
     * @param n_criterios - nº de critérios do problema
     * @param n_opcoes - nº de opções do problema
     * @return normalizada_pesada - matriz normalizada pesada
     */
    public static double[][] normalizapesada(double[][] matriznormalizada, double[] vetorPesos, int n_criterios, int n_opcoes) {
        double[][] normalizada_pesada = new double[n_opcoes][n_criterios];
        for (int j = 0; j < n_criterios; j++) {
            for (int i = 0; i < n_opcoes; i++) {
                normalizada_pesada[j][i] = matriznormalizada[j][i] * vetorPesos[j];
            }
        }
        return normalizada_pesada;
    }

    /**
     * Método para calculo da matriz de distâncias.
     *
     * @param matrizNormalizadaPesada - matriz normalizada pesada
     * @param vetormaxmin - vetor de valores máximos ou minimos
     * @param n_opcoes - nº de opções do problema
     * @param n_criterios - nº de critérios do problema
     * @return matrizDistancia - matriz com a distância de cada valor ao máximo
     * ou minimo
     */
    public static double[][] matrizDistancia(double[][] matrizNormalizadaPesada, double[] vetormaxmin, int n_opcoes, int n_criterios) {
        double[][] matrizDistancia = new double[n_opcoes][n_criterios];
        //ciclo for para calcular a diferença absoluta entre cada celula e o valor minimo ou máximo dessa mesma coluna
        for (int j = 0; j < n_opcoes; j++) {
            for (int i = 0; i < n_criterios; i++) {
                matrizDistancia[j][i] = Math.abs(matrizNormalizadaPesada[j][i] - vetormaxmin[j]);
            }
        }

        return matrizDistancia;
    }

    /**
     * Método para calculo do vetor solução ideal e ideal negativa
     *
     * @param matrizDistancia - matriz distância
     * @param n_opcoes - nº de opções do problema
     * @return vetorSolucao - vetor com a raiz quadrada do somatório dos
     * quadrados de cada linha
     */
    public static double[] vetorSolucao(double[][] matrizdistancia, int n_opcoes) {
        double[] vetorSolucao = new double[n_opcoes];
        double[] quadsoma = new double[n_opcoes];
        //cria um vetor com a soma dos quadrados de cada critério
        for (int i = 0; i < n_opcoes; i++) {
            for (int j = 0; j < n_opcoes; j++) {
                quadsoma[i] += matrizdistancia[j][i] * matrizdistancia[j][i];
            }
        }
        //faz a raiz quadrada da soma anterior
        for (int i = 0; i < n_opcoes; i++) {
            vetorSolucao[i] = Math.sqrt(quadsoma[i]);
        }
        return vetorSolucao;
    }

    /**
     *
     * @param vetorSolucaoIdeal - vetor com s Solução ideal
     * @param vetorSolucaoNegativa - vetor com a Solução Ideal Negativa
     * @param n_opcoes - Numero opções
     * @return vetor final de onde vai ser extraido o maximo e o minimo que
     * corresponde melhor e pior alternativa respetivamente
     */
    public static double[] vetorFinal(double[] vetorSolucaoIdeal, double[] vetorSolucaoNegativa, int n_opcoes) {
        double[] vetorFinal = new double[n_opcoes];
        for (int i = 0; i < n_opcoes; i++) {
            vetorFinal[i] = vetorSolucaoNegativa[i] / (vetorSolucaoNegativa[i] + vetorSolucaoIdeal[i]);
        }
        return vetorFinal;
    }

   
}
